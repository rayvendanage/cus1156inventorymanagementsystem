import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;

//
public class jtable extends JFrame {

	
	private JPanel contentPane;
	private javax.swing.JTable jt1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					jtable frame = new jtable();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public jtable() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 830, 305);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(6, 6, 6, 6));
		setContentPane(contentPane);
		contentPane.setLayout(null);
			
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 50, 780, 150);
		contentPane.add(scrollPane);
		
		Store book =new Store("book");
			
		String[] column= {"Item Code","Item Name","Item Price","Opening Units","Purchases","Sales","Closing Stock","Total Stock Value"};
		
	
		jt1 = new javax.swing.JTable(book.my_db_select(),column);
		scrollPane.setViewportView(jt1);
		}

}