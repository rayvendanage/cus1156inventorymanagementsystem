import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

//
public class StoreDriver {
 
public static void main(String[] args) {

	Scanner input = new Scanner(System.in);
	
	

	
	Store bookstore = new Store("bookstore");
	bookstore.purchase(10002, 50, 01262001);
	bookstore.sell(10001,40, 01262001);
	int menuchoice =0;
		 
		 do {
			
			 
		 System.out.println("Welcome to the school bookstore inventory managemnt system!");
		 System.out.println("To get started please chose a menu item from the list below");
		 
		 System.out.println("------------------Menu------------------");
		 System.out.println("1-Purchase an item");
		 System.out.println("2-Sell an item");
		 System.out.println("3-View current staus of stock");
		 System.out.println("4-Get inventory status between two dates");
		 System.out.println("Press -1 to exit");
		 System.out.println("----------------------------------------");
		 System.out.println("Menu Choice: ");
		 
		 menuchoice = input.nextInt();
			 if(menuchoice==1) {
				 System.out.println("To purchase an item for restock please enter the item code, quantity, and date");
				 System.out.println("Item code: ");
				 int itemcode = input.nextInt();
				 
				/*error handling 
				 *  while(bookstore.itemCodeIsValid(itemcode) == false) {
					 System.out.println("Item code is invalid please enter a valid item code");
					 System.out.println("Item code: ");
					 itemcode = input.nextInt();
				 }
				 */
				 System.out.println("Quantity: ");
				 int quantity = input.nextInt();
				 System.out.println("Date: ");
				 int date = input.nextInt();
				 
				 bookstore.purchase(itemcode, quantity, date);
				 
				 System.out.println("Purchase Successful!");
				 
			 }
			 if(menuchoice==2) {
				 System.out.println("To sell an item please enter the item code, quantity, and date");
				 System.out.println("Item code: ");
				 int itemcode = input.nextInt();
				 System.out.println("Quantity: ");
				 int quantity = input.nextInt();
				 System.out.println("Date: ");
				 int date = input.nextInt();
				 
				 bookstore.sell(itemcode, quantity, date);
				 System.out.println("Sale Successful!");
				 
			 }
			 
			 if(menuchoice ==3) {
				
				 System.out.println("See pop up for the Current Stock Table");
				 System.out.println();
				 EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								jtable frame = new jtable();
								frame.setVisible(true);
								
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				 
		}
			 
		
		 }
		 while(menuchoice != -1);
		 }
 
}