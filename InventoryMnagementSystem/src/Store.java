import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 //
public class Store {
	
	private String storename;

	
	//Constructor initilzing the name of the store
	public Store(String name) {
		this.storename = name;
	}
	
	/*
	 * lookup the price/unit of the item being purchased in SQL from the opening stock table by itemid
	 */
	public int getPricePerUnit(int itemID) {
		int price=0;
		 try {
			
			  Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
			  //System.out.println("Connected With the database successfully");

			  //Crating PreparedStatement object
			  PreparedStatement preparedStatement=connection.prepareStatement("select itemPrice, itemName from openingStock where itemCode= (?)");
			      
			  	//Use prepared statement to use variable in query
			  	preparedStatement.setInt(1, itemID);
			   //Creating Java ResultSet object
		        ResultSet resultSet=preparedStatement.executeQuery();
		       
		        while(resultSet.next()){
		             price=resultSet.getInt("itemPrice");
		             String name = resultSet.getString("itemName");
		             
		             //Printing Results
		            // System.out.println(price+" "+name);
		             
		        }
		        
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");
		}
		 
		 return price;
	}
	
	public int getCurrentTotalStockValue(int itemID) {
		int totalStockValue=0;
		 try {
			
			  Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
			  //System.out.println("Connected With the database successfully");

			  //Crating PreparedStatement object
			  PreparedStatement preparedStatement=connection.prepareStatement("select totalStockValue, itemName from openingStock where itemCode= (?)");
			      
			  	//Use prepared statement to use variable in query
			  	preparedStatement.setInt(1, itemID);
			   //Creating Java ResultSet object
		        ResultSet resultSet=preparedStatement.executeQuery();
		       
		        while(resultSet.next()){
		             totalStockValue=resultSet.getInt("totalStockValue");
		             String name = resultSet.getString("itemName");
		             
		             //Printing Results
		            // System.out.println(totalStockValue+" "+name);
		             
		        }
		        
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");
		}
		 
		 return totalStockValue;
	}
	
	public void updateTotalStockValueInStockInTable(int totalStockValue, int itemID) {
		int updatedTotalStockValue = (getpurchases(itemID)*getPricePerUnit(itemID));
		  try {
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
		//System.out.println("Connected With the database successfully");

		    
		    PreparedStatement preparedStatement=connection.prepareStatement("update stockIn set totalStockValue=? where itemCode=?");
		    preparedStatement.setInt(1,updatedTotalStockValue);
		    preparedStatement.setInt(2,itemID);
		  
		   
		  //Executing Query
		    int i = preparedStatement.executeUpdate();
		    //System.out.println( i + "data inserted successfully");
		      
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");


		}

	}
	
	public void updatePurchasedUnitsInStockInTable(int quantity, int itemID) {
		  try {
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
		//System.out.println("Connected With the database successfully");

		    
		    PreparedStatement preparedStatement=connection.prepareStatement("update stockIn set purchasedUnits=? where itemCode=?");
		    preparedStatement.setInt(1,quantity+getpurchases(itemID));
		    preparedStatement.setInt(2,itemID);
		  
		   
		  //Executing Query
		    int i = preparedStatement.executeUpdate();
		    //System.out.println( i + "data inserted successfully");
		      
		} catch (SQLException e) {
		///System.out.println("Error while connecting to the database");


		}

	}
	
	public void updatePurchaseDateInStockInTable(int date, int itemID) {
		  try {
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
		//System.out.println("Connected With the database successfully");

		    
		    PreparedStatement preparedStatement=connection.prepareStatement("update stockIn set purchaseDate=? where itemCode=?");
		    preparedStatement.setInt(1,date);
		    preparedStatement.setInt(2,itemID);
		  
		   
		  //Executing Query
		    int i = preparedStatement.executeUpdate();
		    //System.out.println( i + "data inserted successfully");
		      
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");


		}

	}
	
	
	public void updatePurchasesInCurrentStockTable(int itemID, int quantity) {
		 try {
				Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
				//System.out.println("Connected With the database successfully");

				    
				    PreparedStatement preparedStatement=connection.prepareStatement("update currentStock set purchases=? where itemCode=?");
				    preparedStatement.setInt(1,getpurchases(itemID));
				    preparedStatement.setInt(2,itemID);
				  
				   
				  //Executing Query
				    int i = preparedStatement.executeUpdate();
				    //System.out.println( i + "data inserted successfully");
				      
				} catch (SQLException e) {
				//System.out.println("Error while connecting to the database");


				}
		 
		
	}
	
	
	
	public int getOpeningUnits(int itemID) {
		int openingUnits=0;
		 try {
			
			  Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
			  //System.out.println("Connected With the database successfully");

			  //Crating PreparedStatement object
			  PreparedStatement preparedStatement=connection.prepareStatement("select openingUnits, itemName from currentStock where itemCode= (?)");
			      
			  	//Use prepared statement to use variable in query
			  	preparedStatement.setInt(1, itemID);
			   //Creating Java ResultSet object
		        ResultSet resultSet=preparedStatement.executeQuery();
		       
		        while(resultSet.next()){
		             openingUnits=resultSet.getInt("openingUnits");
		             String name = resultSet.getString("itemName");
		             
		             //Printing Results
		             //System.out.println(openingUnits+" "+name);
		             
		        }
		        
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");
		}
		 
		 return openingUnits;
	}
	
	public int getpurchases(int itemID) {
		int purchases=0;
		 try {
			
			  Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
			  //System.out.println("Connected With the database successfully");

			  //Crating PreparedStatement object
			  PreparedStatement preparedStatement=connection.prepareStatement("select purchasedUnits, itemName from stockIn where itemCode= (?)");
			      
			  	//Use prepared statement to use variable in query
			  	preparedStatement.setInt(1, itemID);
			   //Creating Java ResultSet object
		        ResultSet resultSet=preparedStatement.executeQuery();
		       
		        while(resultSet.next()){
		             purchases=resultSet.getInt("purchasedUnits");
		             String name = resultSet.getString("itemName");
		             
		             //Printing Results
		             //System.out.println(purchases+" "+name);
		             
		        }
		        
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");
		}
		 
		 return purchases;
	}
	
	public int getSales(int itemID) {
		int sales=0;
		 try {
			
			  Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
			 // System.out.println("Connected With the database successfully");

			  //Crating PreparedStatement object
			  PreparedStatement preparedStatement=connection.prepareStatement("select soldUnits, itemName from stockOut where itemCode= (?)");
			      
			  	//Use prepared statement to use variable in query
			  	preparedStatement.setInt(1, itemID);
			   //Creating Java ResultSet object
		        ResultSet resultSet=preparedStatement.executeQuery();
		       
		        while(resultSet.next()){
		             sales=resultSet.getInt("soldUnits");
		             String name = resultSet.getString("itemName");
		             
		             //Printing Results
		            // System.out.println(sales+" "+name);
		             
		        }
		        
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");
		}
		 
		 return sales;
	}
	
	public void updateSalesInCurrentStockTable(int itemID, int quantity) {
		 try {
				Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
				//System.out.println("Connected With the database successfully");

				    
				    PreparedStatement preparedStatement=connection.prepareStatement("update currentStock set sales=? where itemCode=?");
				    preparedStatement.setInt(1,getSales(itemID));
				    preparedStatement.setInt(2,itemID);
				  
				   
				  //Executing Query
				    int i = preparedStatement.executeUpdate();
				    //System.out.println( i + "data inserted successfully");
				      
				} catch (SQLException e) {
				//System.out.println("Error while connecting to the database");


				}
		 
		
	}
	
	
	
	public void updateClosingStockInCurrentStockTable(int itemID) {
		//closing stock = opening stock + purchases - sales
		int updatedClosingStock = (getOpeningUnits(itemID) + getpurchases(itemID)) - getSales(itemID);
		 
		try {
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
		//System.out.println("Connected With the database successfully");

		    
		    PreparedStatement preparedStatement=connection.prepareStatement("update currentStock set closingStock=? where itemCode=?");
		    preparedStatement.setInt(1,updatedClosingStock);
		    preparedStatement.setInt(2,itemID);
		  
		   
		  //Executing Query
		    int i = preparedStatement.executeUpdate();
		    //System.out.println( i + "data inserted successfully");
		      
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");


		}
	}
	
	public int getSoldUnitsFromStockOutTable(int itemID) {
		int soldUnits=0;
		 try {
			
			  Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
			  //System.out.println("Connected With the database successfully");

			  //Crating PreparedStatement object
			  PreparedStatement preparedStatement=connection.prepareStatement("select soldUnits, itemName from stockOut where itemCode= (?)");
			      
			  	//Use prepared statement to use variable in query
			  	preparedStatement.setInt(1, itemID);
			   //Creating Java ResultSet object
		        ResultSet resultSet=preparedStatement.executeQuery();
		       
		        while(resultSet.next()){
		             soldUnits=resultSet.getInt("soldUnits");
		             String name = resultSet.getString("itemName");
		             
		             //Printing Results
		             //System.out.println(soldUnits+" "+name);
		             
		        }
		        
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");
		}
		 
		 return soldUnits;
	
	}
	
	public void updateTotalStockValueInStockOutTable(int totalStockValue, int itemID) {
		int updatedTotalStockValue = (getSales(itemID)*getPricePerUnit(itemID));
		  try {
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
		//System.out.println("Connected With the database successfully");

		    
		    PreparedStatement preparedStatement=connection.prepareStatement("update stockOut set totalStockValue=? where itemCode=?");
		    preparedStatement.setInt(1,updatedTotalStockValue);
		    preparedStatement.setInt(2,itemID);
		  
		   
		  //Executing Query
		    int i = preparedStatement.executeUpdate();
		    //System.out.println( i + "data inserted successfully");
		      
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");


		}

	}
	
	public void updateSoldUnitsInStockOutTable(int quantity, int itemID) {
		  try {
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
		//System.out.println("Connected With the database successfully");

		    
		    PreparedStatement preparedStatement=connection.prepareStatement("update stockOut set soldUnits=? where itemCode=?");
		    preparedStatement.setInt(1,quantity+getSales(itemID));
		    preparedStatement.setInt(2,itemID);
		  
		   
		  //Executing Query
		    int i = preparedStatement.executeUpdate();
		    //System.out.println( i + "data inserted successfully");
		      
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");


		}

	}
	
	public void updateSoldDateInStockOutTable(int date, int itemID) {
		  try {
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
		//System.out.println("Connected With the database successfully");

		    
		    PreparedStatement preparedStatement=connection.prepareStatement("update stockOut set soldDate=? where itemCode=?");
		    preparedStatement.setInt(1,date);
		    preparedStatement.setInt(2,itemID);
		  
		   
		  //Executing Query
		    int i = preparedStatement.executeUpdate();
		    //System.out.println( i + "data inserted successfully");
		      
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");


		}

	}
	
	public void updateTotalStockValueInCurrentStockTable(int totalStockValue, int itemID) {
		int updatedClosingStock = (getOpeningUnits(itemID) + getpurchases(itemID)) - getSales(itemID);
		int updatedTotalStockValue = (updatedClosingStock*getPricePerUnit(itemID));
		  try {
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");//Establishing connection
		//System.out.println("Connected With the database successfully");

		    
		    PreparedStatement preparedStatement=connection.prepareStatement("update currentStock set totalStockValue=? where itemCode=?");
		    preparedStatement.setInt(1,updatedTotalStockValue);
		    preparedStatement.setInt(2,itemID);
		  
		   
		  //Executing Query
		    int i = preparedStatement.executeUpdate();
		    //System.out.println( i + "data inserted successfully");
		      
		} catch (SQLException e) {
		//System.out.println("Error while connecting to the database");


		}

	}
	
	
		
	public void purchase(int itemID, int quantity, int date) {
		
		
		//lookup the price/unit of the item being purchased in SQL from the opening stock table by itemid
		
		int pricePerUnit = getPricePerUnit(itemID);
		
		//multiply the price/unit by the quantity and whatever we get for that update the stock in table,
		
		int totalStockValue = pricePerUnit*quantity;
		
		updatePurchasedUnitsInStockInTable(quantity,itemID);
		updateTotalStockValueInStockInTable(totalStockValue,itemID);
		
		updatePurchaseDateInStockInTable(date,itemID);
		
		//whenever the stock in the purchases table is updated, the current status of stock table should be updated as well
		//the quantity should be added to the number of existing units in the purchases column, 
		//which will trigger an update of other columns in the current status of stock table
		//this can be done by adding new java methods or if there is a way to do it in SQL 
		updatePurchasesInCurrentStockTable(itemID,quantity);
		updateClosingStockInCurrentStockTable(itemID);
		updateTotalStockValueInCurrentStockTable(totalStockValue,itemID);
		
	}
	
	
	public void sell(int itemID, int quantity,int date) {
		//lookup the price/unit of the item being purchased in SQL from the opening stock table by itemid
		int pricePerUnit = getPricePerUnit(itemID);
		//multiply the price/unit by the quantity and whatever we get for that update the sales/stock out table
		//by in the total stock value column add the answer to the existing stock value in the table for that item
		int totalStockValue = pricePerUnit*quantity;
		
		updateSoldUnitsInStockOutTable(quantity, itemID);
		updateTotalStockValueInStockOutTable(totalStockValue,itemID);
		
		updateSoldDateInStockOutTable(date, itemID);
		
		
		//whenever the stock in the sales table is updated, the current status of stock table should be updated as well
		//the quantity should be added to the number of existing units in the sales column, 
		//which will trigger an update of other columns in the current status of stock table
		//this can be done by adding new java methods or if there is a way to do it in SQL 
		updateSalesInCurrentStockTable(itemID,quantity);
		updateClosingStockInCurrentStockTable(itemID);
		updateTotalStockValueInCurrentStockTable(totalStockValue,itemID);
		
	}
	
	public void checkInventoryBetween(int date1, int date2) {
		
	}
	
	public void checkCurrentInventory() {
		//output the data from the current status of stock table in SQL 
	}
	
	public  String[][] my_db_select() {

String[][] data = new String[6][8]; // [rows][columns]
	
try{  
	Class.forName("com.mysql.jdbc.Driver");  
	Connection con= DriverManager.getConnection("jdbc:mysql://localhost:3306/inventory", "rayven", "root");  
	Statement st=con.createStatement();  
	
ResultSet rs=st.executeQuery("SELECT * FROM currentStock"); 
//Looping to store result in returning array data // 
	int i=0;
	while(rs.next())  {
	 for(int j=0;j<8;j++) {
	 //System.out.print(rs.getString(j+1));
	 data[i][j]=rs.getString(j+1);
	 }
	 //System.out.println();
	 i=i+1;
	}

con.close();  
}catch(Exception e){ System.out.println(e);} 


return data;
	}



}

